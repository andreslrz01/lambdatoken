package org.evol.crypto;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import lombok.Builder;

@Builder
public class Encrypter {
	
	private static final Log LOG = LogFactory.getLog(Encrypter.class);

	private static final String DIGEST_MD5 = "MD5";

	public String encrypt(String token) {
		String message = null;
		MessageDigest mDigest;

		try {
			mDigest = MessageDigest.getInstance(DIGEST_MD5);
			byte[] resultadoHash = mDigest.digest(token.getBytes(StandardCharsets.UTF_8));
			message = DatatypeConverter.printHexBinary(resultadoHash);
		} catch (NoSuchAlgorithmException e) {
			LOG.error(e.getMessage(), e);
		}
		return message;
	}

}
