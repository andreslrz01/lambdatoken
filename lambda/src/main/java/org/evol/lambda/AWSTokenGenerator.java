package org.evol.lambda;

import org.evol.crypto.Encrypter;
import org.evol.token.TokenGenerator;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class AWSTokenGenerator implements RequestHandler<Request, Response> {

	public Response handleRequest(Request input, Context context) {
		TokenGenerator generator = TokenGenerator.builder().build();
		Encrypter encrypter = Encrypter.builder().build();
		return Response.builder().token(encrypter.encrypt(generator.getToken())).build();
	}

}
