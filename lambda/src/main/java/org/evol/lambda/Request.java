package org.evol.lambda;

import lombok.Getter;

@Getter
public class Request {
	private String args[];
}
