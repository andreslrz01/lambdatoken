package org.evol.token;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import lombok.Builder;

@Builder
public class TokenGenerator {
	
	public static final String TOKEN = "-*3mC4$v1+nT1k";
	
	/**
	 * Genera un token en texto plano para enviar como parametro de seguridad 
	 * para el consumo de los webservice
	 * 
	 * @return 
	 */
	public String getToken() {
		ZoneId zone = ZoneId.of( "America/Denver" );
		ZonedDateTime zdt = ZonedDateTime.now(zone);		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHH");
		return zdt.format(formatter) + TOKEN;
	}
	
	
}
